# Open Sand One

> This repository serve resources and examples for my Behance portfolio.

This prototype is a visual overhaul of some webpage, that rely on CSS back drop blur, opacity, shadows and border radiuses. It is mostly inspired by iOS 14 aesthetic and overrides the default website theme (via the xStyle extension).

We reduced a lot of decoration to simplify the design language and emphases the content, and highlighted the website wallpaper − which is customisable by a predefined selection of vector illustrations − in order to enhance the feel of cosiness.

However, we noticed that the combination of backdrop blurriness, transparency and SVG wallpapers had a huge impact on performances, including animation and scrolling. We measured that most integrated GPU where not able to render a fluid experience, especially on High DPI displays.

## Gallery

![Illustration of the Timeline webpage with the water lily theme.](https://khosmon.gitlab.io/Open-Sand-One/readme_assets/Timeline_idle.One.v01.png)

Illustration of the Timeline webpage with the *water lily* theme. Most of the content borders has been removed, background made translucent and blurry.


![Illustration of the Timeline webpage with the Wood theme.](https://khosmon.gitlab.io/Open-Sand-One/readme_assets/Timeline_with_filter.One.v01.png)

Illustration of the Timeline webpage with the *Wood* theme. The filter list has been revamped to looks like a card when expanded and allow the toggle of switches. Each filters shows if it's activated by using a re-enforced visual code.


![Illustration of the My Apps webpage with the Fall theme.](https://khosmon.gitlab.io/Open-Sand-One/readme_assets/MyApps.One.v01.png)

Illustration of the My Apps webpage with the *Fall* theme. The search bar has been lightened and justified to the width of the app grid.


![Illutration of the Timeline webpage](https://khosmon.gitlab.io/Open-Sand-One/juxtaposejs/Timeline_with_filter.Neo.before.1.png)Illustration of the Timeline web page for the Neo ENT.


![](https://khosmon.gitlab.io/Open-Sand-One/readme_assets/Filter_All.one.mov)

Demonstration of animations of the filters card. The card expand and collapse, got a translucent background on hover, and keep it when expanded.

## Installation

1. Get the [xStyle](https://github.com/FirefoxBar/xStyle) extension on your browser. [Chrome](https://chrome.google.com/webstore/detail/xstyle/hncgkmhphmncjohllpoleelnibpmccpj), [Firefox](https://addons.mozilla.org/firefox/addon/xstyle/)
2. Download the [xstyle.css file](https://gitlab.com/Khosmon/Open-Sand-One/-/blob/master/xstyle.css)
3. In your xStyle settings, click `Install From File` and select your downloaded file.
4. Feel free to edit the content to your wish, or reset the domain to your website.

## About

- One, the Panda logo, the Wallpapers and most of the materials are owned by ODE.
- Other content shown (like Qwant Juniors, CCN) are the propriety of their respective owners.
- All portraits and peoples are fictional or AI-generated.
- Font *Calibri* is owned by Microsoft.
